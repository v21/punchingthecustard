import ddf.minim.*;
import ddf.minim.signals.*;
import ddf.minim.analysis.*;
import ddf.minim.effects.*;

import processing.serial.*;

import cc.arduino.*;

Arduino arduino;

PFont font;

Minim minim;
AudioSample powerup;
AudioSample powerup2;
AudioSample powerdown;
AudioSample powerdown2;

boolean playSounds = true;

color hitColor = color(4, 79, 111);
color fgColor = color(4, 79, 111);
color bgColor = color(84, 145, 158);
color punchColor = color(255, 233, 121);
color winningColor = color(132, 255, 121);
color losingColor = color(255, 132, 121);
color drawingColor = color(255, 233, 121);

int p1_pin = 3;
int p2_pin = 6;

int reset_pin = 10;

boolean childMode = false;

int p1_score;
int p2_score;

int minPunchInterval = 100;

int p1_lastPunchTime = 0;
int p2_lastPunchTime = 0;

boolean p1_lastState = true;
boolean p2_lastState = true;

color p1_color;
color p2_color;

float colorLerp = 0.95;

int gamelength = 60 * 1000;
int starttime = 0;

boolean started = false;
boolean finished = false;

boolean reverseMode = false;

int reverseStart = 1 * 60 * 1000;
int reverseEnd = 2 * 60 * 100;

int[] reverseStarts = {15 * 1000, 30 * 1000, 45 * 1000, 70 * 1000 , 80 * 1000};

int reversePeriod = 5 * 1000;

int reverseDecrement = 1;

void setup() {
  size(1280, 800);
  //size(1000, 600);
  
  font = loadFont("FixedsysExcelsiorIIIb-120.vlw");
  
  minim = new Minim(this);
  powerup = minim.loadSample("Powerup.wav");
  powerup2 = minim.loadSample("Powerup_alt.wav");
  powerdown = minim.loadSample("Powerup_down.wav");
  powerdown2 = minim.loadSample("Powerup_down_alt.wav");
  
  arduino = new Arduino(this, "COM4", 57600);
  
  
  
  for (int i = 0; i <= 13; i++)
    arduino.pinMode(i, Arduino.INPUT);
}

void draw() {
  background(bgColor);
  stroke(fgColor);

  if (!finished && !started && arduino.digitalRead(reset_pin) == Arduino.HIGH){
    start();
  }
  if (finished && arduino.digitalRead(reset_pin) == Arduino.HIGH){
   reset(); 
  }
  
  
  if (started && !finished) {  
    if (millis() > starttime + gamelength) {
      finished = true;
    }
    
    reverseMode = false;
    for (int j = 0; j < reverseStarts.length; j++) {
      if ((millis() - starttime < reverseStarts[j] + reversePeriod) && (millis() - starttime > reverseStarts[j])) {
        reverseMode = true;
      }
    }
    
    int i;
    
    i = p1_pin;
    if (arduino.digitalRead(i) == Arduino.LOW){
      if (p1_lastState) {
        //is on, was on
      }
      else {
        //is on, wasn't on
        if (millis() > p1_lastPunchTime + minPunchInterval) {
          if (!childMode) {
            if (!reverseMode){
              p1_score ++;
              if (playSounds)
                powerup.trigger();
            }
            else{
              p1_score -= reverseDecrement;
              if (playSounds)
                powerdown.trigger();
            }
          }
          else {
            p1_score ++;
            if (playSounds)
              powerup.trigger();
          }
          p1_lastState = true;
          p1_lastPunchTime = millis();
          p1_color = punchColor;
          
        }
      }
    }
    else {
      p1_lastState = false;
    }
      
    
    
    
    i = p2_pin;
    if (arduino.digitalRead(i) == Arduino.LOW){
      if (p2_lastState) {
        //is on, was on
      }
      else {
        //is on, wasn't on
        if (millis() > p2_lastPunchTime + minPunchInterval) {
          if (!childMode) {
            if (!reverseMode){
              p2_score ++;
              if (playSounds)
                powerup2.trigger();
            }
            else{
              p2_score -= reverseDecrement;
              if (playSounds)
                powerdown2.trigger();
            }
          }
          else {
            p2_score ++;
              if (playSounds)
                powerup2.trigger();
          }
          p2_lastState = true;
          p2_lastPunchTime = millis();
          p2_color = punchColor;
        }
      }
      
      
    }
    else {
      p2_lastState = false;
      
    }


    if (p1_score > p2_score) {
      p1_color = lerpColor(winningColor, p1_color, colorLerp);
      p2_color = lerpColor(losingColor, p2_color, colorLerp);
    }
    else if (p2_score > p1_score) {
      p1_color = lerpColor(losingColor, p1_color, colorLerp);
      p2_color = lerpColor(winningColor, p2_color, colorLerp);
    }
    else {
      p1_color = lerpColor(drawingColor, p1_color, colorLerp);
      p2_color = lerpColor(drawingColor, p2_color, colorLerp);
    }
    
    
    noStroke();
    fill(p1_color);
    rect(0,0, width * 0.5, height);
      
    
    fill(p2_color);
    rect(width * 0.5,0, width * 0.5, height);
      
    
    stroke(fgColor);
    textFont(font,120);
    fill(fgColor);  
    textAlign(CENTER);
    text(p1_score, width * 0.25, height * 0.5);
    text(p2_score, width * 0.75, height * 0.5);
  
    int timeleft = gamelength - (millis() - starttime);
    text((timeleft / 1000) + 1, width * 0.5, 100);
    
    if (!childMode) {
      if (!reverseMode) {
        text("PUNCH!", width * 0.5, height - 100);
      }
      else {
        text("DON'T PUNCH!", width * 0.5, height - 100);
      }
    }
    else {
      text("PUNCH!", width * 0.5, height - 100);
    }
  }
  else if (finished) {
    noStroke();
    fill(p1_color);
    rect(0,0, width * 0.5, height);
      
    
    fill(p2_color);
    rect(width * 0.5,0, width * 0.5, height);

    stroke(fgColor);
    textFont(font,120);
    fill(fgColor);  
    textAlign(CENTER);
    text(p1_score, width * 0.25, height * 0.5);
    text(p2_score, width * 0.75, height * 0.5);
    
    if (p1_score > p2_score) {
      text("WINNER", width * 0.25, 100);
      text("LOSER", width * 0.75, 100);
    }
    else if (p2_score > p1_score) {
      
      text("LOSER", width * 0.25, 100);
      text("WINNER", width * 0.75, 100);
    }
    else {
      
      text("TIED", width * 0.25, 100);
      text("TIED", width * 0.75, 100);
    }
      
      
    text("GAME OVER", width * 0.5, height - 100);
    
  }
  else { //not started or finished
    stroke(fgColor);
    textFont(font,120);
    fill(fgColor);  
    textAlign(CENTER);
    text("PUNCH\nTHE CUSTARD", width * 0.5, height * 0.5);
  }
}

void start(){
      finished = false;
      started = true;
      starttime = millis();
}

void reset(){
  finished = false;
    started = false;
    p1_score = 0;
    p2_score = 0;
}

void keyPressed() {
  if (key == 'r') {
    reset();
  }
  if (key == ' ') {
    if (!finished && !started) {
      start();
    }
  }
  if (key == '1') {
    childMode = true;
  }
  if (key == '2') {
    childMode = false;
  }
  
  if (key == 'q') {
    reverseMode = ! reverseMode;
  }
  
}

void stop()
{
  // the AudioPlayer you got from Minim.loadFile()
  powerup.close();
  powerup2.close();
  powerdown.close();
  powerdown2.close();
  
  minim.stop();
  
  // this calls the stop method that
  // you are overriding by defining your own
  // it must be called so that your application
  // can do all the cleanup it would normally do
  super.stop();
}
